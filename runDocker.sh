docker stop tp2jg
docker container rm tp2jg
docker volume rm tp2jg_vol
docker image rm tp2jg_image
docker build -t tp2jg_image -f ./project/docker/Dockerfile .
docker volume create --name tp2jg_vol --opt device=$PWD --opt o=bind --opt type=none
docker run -d -p 5555:5555 --mount source=tp2jg_vol,target=/mnt/app --name tp2jg tp2jg_image

