coverage run --source=. -m unittest discover -s project -v
if [ $? -ne 0 ]
then
	echo "unittest failed, aborting"
	exit 1
fi
./runDocker.sh
sleep 20
python3 -m unittest discover -s project -v -p dtest_*.py
