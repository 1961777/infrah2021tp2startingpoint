#!/bin/bash

export PYTHONPATH=$PWD/project/
echo python path is: 
echo $PYTHONPATH 

echo running the following python program 
echo $@ 

apt update
apt -y install rsyslog
service rsyslog start
python3 "$@"
