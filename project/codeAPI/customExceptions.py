class InitialUserException(Exception):
        pass

class NonExistingUserException(Exception):
        pass

#TODO add new custom exception classes here as/if you need them
class UserAlreadyExistException(Exception):
	pass
