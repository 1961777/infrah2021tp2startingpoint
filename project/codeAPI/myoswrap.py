import subprocess

def runCommandToAddUser(username):
	subprocess.run(['useradd', username])

def runCommandToRemoveUser(username):
	subprocess.run(['userdel', username])

def readFileByLine(filename):
	content = []
	with open(filename) as logs:
		content = logs.readlines()
	return content
