from flask import *
from codeAPI import myoswrap
from codeAPI.customExceptions import *

app = Flask('TP2 API')


def getUserList():
	users = []
	for line in myoswrap.readFileByLine('/etc/passwd'):
		users.append(line.split(':')[0])
	return users

#TODO fill this up properly
INITIAL_USERS = ['root',
        'daemon',
        'bin',
        'sys',
        'sync',
        'games',
        'man',
        'lp',
        'mail',
        'news',
        'uucp',
        'proxy',
        'www-data',
        'backup',
        'list',
        'irc',
        'gnats',
        'nobody',
        'systemd-network',
        'systemd-resolve',
        'systemd-timesync',
        'messagebus',
        'syslog',
        '_apt',
        'tss',
        'uuidd',
        'tcpdump',
        'landscape',
        'pollinate',
        'sshd',
        'systemd-coredump',
        'lubuntu',
        'lxd',
        'vboxadd',
	'Debian-exim']

def isInitialUser(username):
	if username in INITIAL_USERS:
		return True
	return False



#Done classical method to back related route (unit test this)
def hello():
	return 'Welcome to user hot program!'

#Done route method (test with deployment tests)
@app.route('/')
def route_hello(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':hello()})



#classical method to back related route
def getUsers():
	NewUsers = []
	Users = getUserList()
	for user in Users:
		if (isInitialUser(user) == False):
			NewUsers.append(user)
	users = {'InitialUsers':INITIAL_USERS,'NewUsers':NewUsers}
	return users

#route method
@app.route('/getusers')
def route_getUsers(): #pragma: no cover

	return jsonify({'code':'2000','msg':getUsers()})




#done classical method to back related route
def delUser(username):
	if username in getUserList():
		if isInitialUser(username):
			raise InitialUserException('cannot delete an initial user.')

		else:
			myoswrap.runCommandToRemoveUser(username)
			return 'deleted ' + username
	raise NonExistingUserException('The specified user (' + username + ') does not exist, cannot perform delete operation.')

#done route method
@app.route('/deluser')
def route_delUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = delUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except InitialUserException as iue:
			return make_response(jsonify({'code':'1003', 'msg':str(iue)}), 400)
		except NonExistingUserException as neue:
			return make_response(jsonify({'code':'1002', 'msg':str(neue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for deletion.'}), 400)




#classical method to back related route
def resetUsers():
	users = getUserList()
	nbUserDeleted = 0
	for user in users:
		if isInitialUser(user) == False:
			delUser(user)
			nbUserDeleted = nbUserDeleted + 1
	return nbUserDeleted

#route method
@app.route('/resetusers')
def route_resetUsers(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':resetUsers()})

#classical method to back related route
def addUser(username):
	exceptionRaised = 0
	for user in getUserList():
		if(username == user):
			exceptionRaised = 1
			raise UserAlreadyExistException('The username ' + username + ' already exist. The username must be unique.')
	if exceptionRaised == 0:
		myoswrap.runCommandToAddUser(username)
		return 'user ' + username + ' has been created.'

#route method
@app.route('/adduser')
def route_addUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = addUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except UserAlreadyExistException as uae:
			return make_response(jsonify({'code':'1001','msg':str(uae)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for creation'}), 400)

#classical method to back related route
def getLogs():
	logs = []
	for line in myoswrap.readFileByLine('/var/log/auth.log'):
		logs.append(line)
	return logs

#route method 
@app.route('/getlog')
def route_getLogs(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':getLogs()})




if __name__ == "__main__": #pragma: no cover
	app.run(debug=True, host='0.0.0.0', port=5555)

