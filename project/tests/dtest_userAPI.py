import unittest
import requests
import json
from codeAPI import userAPI


IP = "127.0.0.1"
PORT = "5555"
URL = "http://" + IP + ":" + PORT + "/"

class BasicTests(unittest.TestCase):

	def test_hello(self):
		response = requests.get(URL)
		self.assertEqual(200, response.status_code)

		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		self.assertIn('msg', obj)

	def test_delUser(self):
		response = requests.get(URL + "adduser?username=Roy")
		self.assertEqual(200, response.status_code)

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertIn('Roy', obj['msg']['NewUsers'])

		response = requests.get(URL + "deluser?username=Roy")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertNotIn('Roy', obj['msg']['NewUsers'])

	def test_delUserNoParam(self):
		response = requests.get(URL + "deluser")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1000', obj['code'])

	def test_delUserNonExisting(self):
		response = requests.get(URL + "deluser?username=Brodeur")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1002', obj['code'])

	def test_delInitialUser(self):
		response = requests.get(URL + "deluser?username=root")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1003', obj['code'])


	def test_getUsers(self):
		response = requests.get(URL + "getusers")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual(userAPI.INITIAL_USERS,obj['msg']['InitialUsers'])
		self.assertEqual([],obj['msg']['NewUsers'])

		response = requests.get(URL + "adduser?username=TestGetUsers")
		self.assertEqual(200, response.status_code)

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertIn('TestGetUsers', obj['msg']['NewUsers'])
		self.assertEqual('2000', obj['code'])

		response = requests.get(URL + "deluser?username=TestGetUsers")

	def test_addUsers(self):
		response = requests.get(URL + "adduser?username=TestAddUsers")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertIn('TestAddUsers', obj['msg']['NewUsers'])

		response = requests.get(URL + "deluser?username=TestAddUsers")

	def test_addUsers_whenNoUsername(self):
		response = requests.get(URL + "adduser")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1000', obj['code'])

	def test_addUsers_whenUserExist(self):
		response = requests.get(URL + "adduser?username=root")
		self.assertEqual(400,response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1001', obj['code'])

	def test_resetUser(self):
		response = requests.get(URL + "adduser?username=TestAddUsers")
		response = requests.get(URL + "resetusers")
		self.assertEqual(200,response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		self.assertEqual(1, obj['msg'])

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertNotIn('TestAddUsers', obj['msg']['NewUsers'])

	#_1_ so it is called before the others
	def test_1_getLog(self):
		response = requests.get(URL + "adduser?username=TestAddUsers")
		response = requests.get(URL + "getlog")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		#2 because 1 log for the create and 1 for the group
		self.assertEqual(2, len(obj['msg']))
		response = requests.get(URL + "deluser?username=TestAddUsers")
		response = requests.get(URL + "getlog")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		#5 because the 2 of creation, 2 of deletion and 1 of shadowing
		self.assertEqual(5, len(obj['msg']))

if __name__ == '__main__':
	unittest.main()
