import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI


class BasicTests(unittest.TestCase):

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		obj = userAPI.delUser('Roy')
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUserNonExisting(self, mock_oswrap):
		with self.assertRaises(NonExistingUserException) as context:
			userAPI.delUser('Brodeur')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delInitialUser(self, mock_oswrap):
		with self.assertRaises(InitialUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			userAPI.delUser('root')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	def test_isInitialUserWhenIsOne(self):
		expected = True
		initialUser = 'root'
		received = userAPI.isInitialUser(initialUser)
		self.assertTrue(expected == received)

	def test_isInitialUserWhenIsNotOne(self):
		expected = False
		initialUser = 'NotMuchOfAUser'
		received = userAPI.isInitialUser(initialUser)
		self.assertTrue(expected == received)

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserList(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
		expected = ['root']
		received = userAPI.getUserList()
		self.assertEqual(received[0], expected[0])


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getLogs(self, mock_oswrap):
		expected = ["Mar  9 22:19:35 473d3d237f7c useradd[971]: new group: name=jean, GID=1000\n"]
		mock_oswrap.readFileByLine.return_value = expected
		received = userAPI.getLogs()
		self.assertEqual(received[0], expected[0])

	def test_getUsers_whenOnlyInitials(self):
		expected_json = {'InitialUsers':userAPI.INITIAL_USERS, 'NewUsers': []}
		received = userAPI.getUsers()
		self.assertEqual(received, expected_json)

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsers_whenNewUsersAdded(self, mock_oswrap):
		expected_json = {'InitialUsers':userAPI.INITIAL_USERS, 'NewUsers': ['bob']}
		mock_oswrap.readFileByLine.return_value = ['bob']
		received = userAPI.getUsers()
		self.assertEqual(received, expected_json)

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsers_whenMultipleNewUsersAdded(self, mock_oswrap):
		expected_json = {'InitialUsers':userAPI.INITIAL_USERS, 'NewUsers': ['bob','jean']}
		mock_oswrap.readFileByLine.return_value = ['bob', 'jean']
		received = userAPI.getUsers()
		self.assertEqual(received, expected_json)

	def test_getResetUsers(self):
		received = userAPI.resetUsers()
		self.assertEqual(received, 0)

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getResetUsers_whenNewUsersPopulated(self, mock_oswrap):
		expected = 2
		mock_oswrap.readFileByLine.return_value = ['bob', 'jean']
		received = userAPI.resetUsers()
		self.assertEqual(received, expected)
		mock_oswrap.runCommandToRemoveUser.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUsers(self,mock_oswrap):
		username = 'jean'
		expected = 'user ' + username + ' has been created.'
		received = userAPI.addUser(username)
		self.assertEqual(received, expected)
		mock_oswrap.runCommandToAddUser.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUsers_whenUserExist(self,mock_oswrap):
		with self.assertRaises(UserAlreadyExistException) as context:
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			username = 'root'
			received = userAPI.addUser(username)
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.runCommandToAddUser.assert_not_called()

if __name__ == '__main__':
	unittest.main()

